﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RobotAlertasMidY
{
    class Program
    {
        static void Main(string[] args)
        {
            var configMail = new model.ValoresConfigMail();
            var robotPMPmidy = new model.RoboPMPMidY(configMail);

            util.LogCustom.Instance.Log("Se inicia proceso: Mails de Alerta PMP");
            
            robotPMPmidy.Run();

            util.LogCustom.Instance.Log("Ha terminado el proceso: Mails de Alerta PMP");


            Console.ReadLine();
        }
    }
}
