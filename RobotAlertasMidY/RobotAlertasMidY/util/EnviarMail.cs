﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;

namespace RobotAlertasMidY.util
{
   public class EnviarMail
    {
       interfaces.IProviderConnectionStaticMail Connection;
       public EnviarMail(interfaces.IProviderConnectionStaticMail _Connect)
       {
           this.Connection = _Connect;
       }
       //interfaces.IProviderContentMail 
       public void Enviar(interfaces.IProviderContentDynamicMail dataMail)
       {
           String Asunto = dataMail.Asunto;

           List<System.Net.Mail.MailMessage> mensajes = new List<System.Net.Mail.MailMessage>();

           /*mensajes*/
           try
           {

               SmtpClient smtpMail;

               //---Verificar servidor smtp y puertos
               if (this.Connection.Port != string.Empty)
               {
                   smtpMail = new SmtpClient(this.Connection.SMTP_Address, Int32.Parse(this.Connection.Port));
               }
               else
               {
                   smtpMail = new SmtpClient(this.Connection.SMTP_Address);
               }
               //----------------------
               MailAddress _from = new MailAddress(this.Connection.From, this.Connection.FromName);//, "");
               MailMessage _mensaje = new MailMessage();

               _mensaje.Body = dataMail.Body;
               _mensaje.Subject = dataMail.Asunto;
               _mensaje.From = _from;
               _mensaje.IsBodyHtml = dataMail.IsHtmlBody;
               foreach (string unDestino in dataMail.To)
               {

                   
                           _mensaje.To.Add(unDestino);
                      
               }

               //--------------
            
                   smtpMail.EnableSsl = this.Connection.EnableSsl;
              

               //------------Verficar Credenciales
               if (this.Connection.User == string.Empty || this.Connection.Password == string.Empty)
                   smtpMail.UseDefaultCredentials = true;
               else
                   smtpMail.Credentials = new System.Net.NetworkCredential(this.Connection.User, this.Connection.Password);
               //-----------------------------------------------------


               smtpMail.Send(_mensaje);
             
           }
           catch (Exception err)
           {
               //Console.WriteLine("> Error al enviar E-Mail: " + err.Message);
               //ObjectLogger.Log("Error al enviar E-Mail: " + err.Message, TargetLog.Console_File, err);
               //LogFile.logger.LogForNet.Info("ERROR AL ENVIAR E-MAIL: " + err.Message);
           }

           //Console.WriteLine("Para: {0}", dataMail.To);
           //Console.WriteLine("Asunto: {0}", dataMail.Asunto);
           //Console.WriteLine("Mensaje: {0}", dataMail.Body);
           //Console.WriteLine("----");
       }

    }
}
