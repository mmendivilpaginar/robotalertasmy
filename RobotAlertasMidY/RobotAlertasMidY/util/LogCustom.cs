﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RobotAlertasMidY.util
{
   public  class LogCustom:interfaces.ILogCustom
    {


        private  LogCustom ()
	    {

	    }
        private static interfaces.ILogCustom _Instance;
        public static interfaces.ILogCustom Instance
        {
            get
            {
                if (_Instance == null) _Instance = new LogCustom();
                return _Instance;
            }
        }


       
       
       #region ILogCustom Members
        public void Log(string text)
        {
            Console.WriteLine(text);
        }

        #endregion
    }
}
