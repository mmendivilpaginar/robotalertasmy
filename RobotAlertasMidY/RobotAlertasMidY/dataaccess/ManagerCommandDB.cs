﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace RobotAlertasMidY.dataaccess
{
    public class ManagerCommandDB
    {

        private string GetConnectionString()
        {

            string cnnst = ConfigurationManager.ConnectionStrings["ConnectionDBSCJ"].ConnectionString.ToString();

            return cnnst;

        }
        public DataTable RunStoreProcedure(string nombreSP, List<SqlParameter> parametros)
        {
            string Conexion;
            Conexion = this.GetConnectionString();
            SqlConnection conn = new SqlConnection(Conexion);
            conn.Open();
            SqlCommand command = new SqlCommand(nombreSP, conn);

            command.CommandType = CommandType.StoredProcedure;

            foreach (SqlParameter item in parametros)
            {
                command.Parameters.Add(item);
            }
            //------------------------------------------------------
            DataTable resultadosTable = null;
            try
            {
                SqlDataReader resultadoReader = command.ExecuteReader();
                resultadosTable = new DataTable();
                resultadosTable.Load(resultadoReader);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                conn.Close();
                conn.Dispose();
            }

            return resultadosTable;

        }



        public DataTable RunQuery(string query, List<SqlParameter> parametros)
        {
            string Conexion;
            Conexion = this.GetConnectionString();
            SqlConnection conn = new SqlConnection(Conexion);
            conn.Open();
            SqlCommand command = new SqlCommand(query, conn);

            // command.CommandType = CommandType.Text;



            foreach (SqlParameter item in parametros)
            {
                command.Parameters.Add(item);
            }
            //------------------------------------------------------
            DataTable resultadosTable = null;
            try
            {
                SqlDataReader resultadoReader = command.ExecuteReader();
                resultadosTable = new DataTable();
                resultadosTable.Load(resultadoReader);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                conn.Close();
                conn.Dispose();
            }

            return resultadosTable;
        }




      

    }
}
