﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace RobotAlertasMidY.dataaccess
{
   public class RepositoryQuery
    {

       private ManagerCommandDB _ManagerDB = null;
       private ManagerCommandDB ManagerDB
       {
           get
           {
               if (_ManagerDB == null) _ManagerDB = new ManagerCommandDB();

               return _ManagerDB;
           }
       }
       public RepositoryQuery()
       {
         
       }

       public DataTable GetInfoParaEvaluador(string PeriodoID)
       {
           string query_ForEvaluador = 
            "select dbo.getestadoidevaluacion(e.legajo,e.periodoid,e.tipoformularioid) EstadoPMP, e.Legajo, e.PeriodoId, e.TipoFormularioId, " +
            "e.Nombre, e.Apellido, re.PasoID, re.LegajoEvaluador, u.UsuarioID, u.Interno, u.Email "+
            "from eval_evaluados e join eval_relevaluadosevaluadores re "+
            "on (e.legajo= re.legajo and e.TipoformularioID= re.TipoformularioID and e.periodoid= re.periodoid) "+
            "inner join usuario u on re.Legajoevaluador = u.Legajo and estado=2 "+
            "where e.periodoid=" + PeriodoID + " and dbo.getestadoidevaluacion(e.legajo,e.periodoid,e.tipoformularioid) = 1 " +
            "and pasoid=dbo.getestadoidevaluacion(e.legajo,e.periodoid,e.tipoformularioid) " + 
            "and (select estado from usuario where usuario.legajo=e.legajo)=2 " +
            "and re.legajoevaluador<>0";

           List<SqlParameter> myParameters=new List<SqlParameter>();
           // Aqui cargas los parametrossi los necesita.
           //myParameters.Add(
           DataTable resultTable=this.ManagerDB.RunQuery(query_ForEvaluador, myParameters);

           return resultTable;
       }

       public DataTable GetInfoParaEvaluados(string PeriodoID)
       {
           string query_ForEvaluador =
             "select dbo.getestadoidevaluacion(e.legajo,e.periodoid,e.tipoformularioid), "+
             "e.PeriodoId, e.Legajo, e.PeriodoId, e.TipoFormularioId, u.Nombre, u.Apellido, u.Email "+
             "from eval_evaluados e inner join usuario u on (e.Legajo = u.Legajo and u.estado=2) " +
                     " inner join eval_RelEvaluadosEvaluadores on eval_RelEvaluadosEvaluadores.Legajo=e.legajo"+
                      " and eval_RelEvaluadosEvaluadores.Periodoid=e.PeriodoID"+
                      " and eval_RelEvaluadosEvaluadores.TipoFormularioID=e.TipoFormularioID "+
                      " and pasoid=1 " +
             "where e.periodoid=" + PeriodoID + "  and dbo.getestadoidevaluacion(e.legajo,e.periodoid,e.tipoformularioid) in (-1,2) and legajoevaluador<>0";

           List<SqlParameter> myParameters = new List<SqlParameter>();
           // Aqui cargas los parametrossi los necesita.
           //myParameters.Add(
           DataTable resultTable = this.ManagerDB.RunQuery(query_ForEvaluador, myParameters);
           return resultTable;
       }

       public DataTable GetInfoParaEvaluadosOP(string PeriodoID)
       {
           string query_ForEvaluador =
             "select dbo.getestadoidevaluacion(e.legajo,e.periodoid,e.tipoformularioid), " +
             "e.PeriodoId, e.Legajo, e.PeriodoId, e.TipoFormularioId, u.Nombre, u.Apellido, u.Email " +
             "from eval_evaluados e inner join usuario u on e.Legajo = u.Legajo and estado=2 " +
             "where e.periodoid=" + PeriodoID + " and dbo.getestadoidevaluacion(e.legajo,e.periodoid,e.tipoformularioid) =2 " +
             "and (select estado from usuario where usuario.legajo=e.legajo)=2";

           List<SqlParameter> myParameters = new List<SqlParameter>();
           // Aqui cargas los parametrossi los necesita.
           //myParameters.Add(
           DataTable resultTable = this.ManagerDB.RunQuery(query_ForEvaluador, myParameters);
           return resultTable;
       }

       public DataTable GetInfoParaEvaluadorOP(string PeriodoID)
       {
           string query_ForEvaluador =
            "select dbo.getestadoidevaluacion(e.legajo,e.periodoid,e.tipoformularioid) EstadoPMP, e.Legajo, e.PeriodoId, e.TipoFormularioId, " +
            "e.Nombre, e.Apellido, re.PasoID, re.LegajoEvaluador, u.UsuarioID, u.Interno, u.Email " +
            "from eval_evaluados e join eval_relevaluadosevaluadores re " +
            "on (e.legajo= re.legajo and e.TipoformularioID= re.TipoformularioID and e.periodoid= re.periodoid) " +
            "inner join usuario u on re.Legajoevaluador = u.Legajo and estado=2 " +
            "where e.periodoid=" + PeriodoID + " and dbo.getestadoidevaluacion(e.legajo,e.periodoid,e.tipoformularioid) in (-1,1) " +
            "and pasoid=1 " +
            "and (select estado from usuario where usuario.legajo=e.legajo)=2"; 

           List<SqlParameter> myParameters = new List<SqlParameter>();
           // Aqui cargas los parametrossi los necesita.
           //myParameters.Add(
           DataTable resultTable = this.ManagerDB.RunQuery(query_ForEvaluador, myParameters);

           return resultTable;
       }

       public DataTable GetInfoParaAuditorOP(string PeriodoID)
       {
           string query_ForEvaluador =
            "select dbo.getestadoidevaluacion(e.legajo,e.periodoid,e.tipoformularioid) EstadoPMP, e.Legajo, e.PeriodoId, e.TipoFormularioId, " +
            "e.Nombre, e.Apellido, re.PasoID, re.LegajoEvaluador, u.UsuarioID, u.Interno, u.Email " +
            "from eval_evaluados e join eval_relevaluadosevaluadores re " +
            "on (e.legajo= re.legajo and e.TipoformularioID= re.TipoformularioID and e.periodoid= re.periodoid) " +
            "inner join usuario u on re.Legajoevaluador = u.Legajo and estado=2 " +
            "where e.periodoid=" + PeriodoID + " and dbo.getestadoidevaluacion(e.legajo,e.periodoid,e.tipoformularioid)=3 " +
            "and pasoid=3 "+
            "and (select estado from usuario where usuario.legajo=e.legajo)=2"; 

           List<SqlParameter> myParameters = new List<SqlParameter>();
           // Aqui cargas los parametrossi los necesita.
           //myParameters.Add(
           DataTable resultTable = this.ManagerDB.RunQuery(query_ForEvaluador, myParameters);

           return resultTable;
       }
    }
}
