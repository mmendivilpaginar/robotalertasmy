﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RobotAlertasMidY.model
{
    /// <summary>
    /// Implementacion para los datos dinamicos de un mail
    /// </summary>
    class ItemForSend:interfaces.IProviderContentDynamicMail
    {
        private string _body;
        public string Body
        {
            get { return _body; }
        }

        public string[] _to;
        public string[] To
        {
            get { return _to; }
        }

        private string _asunto;
        public string Asunto
        {
            get { return _asunto; }
        }

        public bool IsHtmlBody
        {
            get { return true; }
        }

        public ItemForSend(string to, string asunto, string body)
        {
            this._asunto = asunto;
            this._to = new string[] { to };
            this._body= body;

        }
    }
}
