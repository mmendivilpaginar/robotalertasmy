﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Configuration;

namespace RobotAlertasMidY.model
{
    class ReportForEvaluado:BaseReportFor
    {

        public override IEnumerable<interfaces.IProviderContentDynamicMail> GetBodyMail(int PeriodoID)
        {
            var myList = new List<ItemForSend>();

            DataTable dtEvaluado = this.MyDataAccess.GetInfoParaEvaluados(PeriodoID.ToString());
            ValoresConfigMail valores = new ValoresConfigMail();

            foreach (DataRow dr in dtEvaluado.Rows)
            {
               
                string MailTo="";

                if (this.IsTestMode)
                    MailTo = this.GetEmail_Test();
                else
                    MailTo = dr["Email"].ToString();

                string Asunto = "Evaluación de desempeño de Mitad de Año - Status - Evaluación a Completar";

                    
                string Link = valores.SiteUrl + "/servicios/form_evaldesemp.aspx?openPop=2&Legajo=" + dr["Legajo"].ToString() + "&PeriodoID=" + dr["PeriodoId"].ToString() + "&TipoFormularioID=" + dr["TipoFormularioId"].ToString();
                Link = "<a href=\"" + Link + "\">Acceder a la PMP</a>";
                StringBuilder Body = new StringBuilder();

                Body.Append("<Table>");

                Body.Append("<tr>");
                Body.Append("<td style=\"padding-bottom: 10px;\">");
                Body.Append("Estimado/a");
                Body.Append("</td>");
                Body.Append("</tr>");

                Body.Append("<tr>");
                Body.Append("<td style=\"padding-bottom: 10px;\">");
                Body.Append("Le comunicamos que su evaluación aún permanece en su poder. Por favor complete los campos necesarios para");
                Body.Append(" darle curso a la misma.");
                Body.Append("</td>");
                Body.Append("</tr>");

                //Body.Append("<tr>");
                //Body.Append("<td style=\"padding-bottom: 10px;\">");
               
                //Body.Append("</td>");
                //Body.Append("</tr>");

                Body.Append("<tr>");
                Body.Append("<td style=\"padding-bottom: 10px;\">");
                Body.Append(Link);
                Body.Append("</td>");
                Body.Append("</tr>");

                Body.Append("<tr>");
                Body.Append("<td style=\"padding-bottom: 10px;\">");
                Body.Append("Finalización del proceso PMP MYPR: 15/03/2012.");
                Body.Append("</td>");
                Body.Append("</tr>");

                Body.Append("<tr>");
                Body.Append("<td>");
                Body.Append("Muchas gracias.");
                Body.Append("</td>");
                Body.Append("</tr>");

                Body.Append("<tr>");
                Body.Append("<td>");
                Body.Append("Recursos Humanos.");
                Body.Append("</td>");
                Body.Append("</tr>");

                Body.Append("<tr>");
                Body.Append("<td>");
                Body.Append("&nbsp;"); 
                Body.Append("</td>");
                Body.Append("</tr>");

                Body.Append("<tr>");
                Body.Append("<td>");
                Body.Append("&nbsp;"); 
                Body.Append("</td>");
                Body.Append("</tr>");

                Body.Append("<tr>");
                Body.Append("<td style=\"padding-bottom: 10px; color: #888888; \">");
                Body.Append("Este mensaje fue enviado desde la Intranet de SCJ Conosur, no debe responder al mismo.");
                Body.Append("</td>");
                Body.Append("</tr>");

                Body.Append("</Table>");
                

                myList.Add(new ItemForSend(MailTo, Asunto, Body.ToString()));     

            }
           
            return myList;
        }
    }
}
