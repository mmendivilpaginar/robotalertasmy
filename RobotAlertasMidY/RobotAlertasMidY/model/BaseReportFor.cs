﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RobotAlertasMidY.interfaces;
using RobotAlertasMidY.dataaccess;
using System.Configuration;

namespace RobotAlertasMidY.model
{
   abstract class BaseReportFor
    {
       protected Boolean IsTestMode
       {
           get
           {
               string ModoTest = ConfigurationManager.AppSettings["Test_ModoEjecucion"].ToString();
               if (ModoTest == "1")
                   return true; 
               else
                   return false;
           }
       }

       protected string GetEmail_Test()
       {
           string mail_test = ConfigurationManager.AppSettings["Test_MailDestino"].ToString();
           return mail_test;
       }
       protected RepositoryQuery MyDataAccess = new RepositoryQuery();
       public abstract IEnumerable<IProviderContentDynamicMail> GetBodyMail(int PeriodoID);
    }
}
