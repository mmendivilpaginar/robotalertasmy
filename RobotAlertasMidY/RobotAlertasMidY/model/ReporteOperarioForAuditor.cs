﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RobotAlertasMidY.interfaces;
using System.Data;
using System.Configuration;

namespace RobotAlertasMidY.model
{
    class ReporteOperarioForAuditor : BaseReportFor
    {
        public override IEnumerable<IProviderContentDynamicMail> GetBodyMail(int PeriodoID)
        {
            var myList = new List<ItemForSend>();

            DataTable dtEvaluador = this.MyDataAccess.GetInfoParaAuditorOP(PeriodoID.ToString());
            ValoresConfigMail valores = new ValoresConfigMail();

            foreach (DataRow dr in dtEvaluador.Rows)
            {
                //string ModoTest = ConfigurationManager.AppSettings["ConexionDefault"].ToString();

                string MailTo = "";

                if (this.IsTestMode)
                    MailTo = ConfigurationManager.AppSettings["Test_MailDestinoAuditor"].ToString();
                else
                    MailTo = dr["Email"].ToString();




                string Link = valores.SiteUrl + "/servicios/form_evaldesemp.aspx?openPop=OPERARIOSAU&Legajo=" + dr["Legajo"].ToString() + "&PeriodoID=" + dr["PeriodoId"].ToString() + "&TipoFormularioID=" + dr["TipoFormularioId"].ToString();
                Link = "<a href=\"" + Link + "\">Acceder a la PMP</a>";
                string Asunto = "Evaluación de desempeño - Status - Evaluación a Completar";

                string NombreEvaluado = dr["Nombre"].ToString() + " " + dr["Apellido"].ToString();

                StringBuilder Body = new StringBuilder();


                Body.Append("<Table>");

                Body.Append("<tr>");
                Body.Append("<td style=\"padding-bottom: 10px;\">");
                Body.Append("Estimado/a");
                Body.Append("</td>");
                Body.Append("</tr>");

                Body.Append("<tr>");
                Body.Append("<td style=\"padding-bottom: 10px;\">");
                Body.Append("Le comunicamos que la evaluación de ");
                Body.Append(NombreEvaluado);
                Body.Append(" se encuentra a la espera de su revisión. Por favor");
                Body.Append(" complete los campos necesarios para darle curso a la misma.");
                Body.Append("</td>");
                Body.Append("</tr>");

                //Body.Append("<tr>");
                //Body.Append("<td style=\"padding-bottom: 10px;\">");

                //Body.Append("</td>");
                //Body.Append("</tr>");
                Body.Append("<tr>");
                Body.Append("<td style=\"padding-bottom: 10px;\">");
                Body.Append(Link);
                Body.Append("</td>");
                Body.Append("</tr>");

                Body.Append("<tr>");
                Body.Append("<td style=\"padding-bottom: 10px;\">");
                Body.Append("Finalización del proceso PMP FYPR: 15/08/2012.");
                Body.Append("</td>");
                Body.Append("</tr>");



                Body.Append("<tr>");
                Body.Append("<td>");
                Body.Append("Muchas gracias.");
                Body.Append("</td>");
                Body.Append("</tr>");

                Body.Append("<tr>");
                Body.Append("<td>");
                Body.Append("Recursos Humanos.");
                Body.Append("</td>");
                Body.Append("</tr>");

                Body.Append("<tr>");
                Body.Append("<td>");
                Body.Append("&nbsp;");
                Body.Append("</td>");
                Body.Append("</tr>");

                Body.Append("<tr>");
                Body.Append("<td>");
                Body.Append("&nbsp;");
                Body.Append("</td>");
                Body.Append("</tr>");

                Body.Append("<tr>");
                Body.Append("<td style=\"padding-bottom: 10px; color: #888888; \">");
                Body.Append("Este mensaje fue enviado desde la Intranet de SCJ Conosur, no debe responder al mismo.");
                Body.Append("</td>");
                Body.Append("</tr>");

                Body.Append("</Table>");

                var nuevoItem = new ItemForSend(MailTo, Asunto, Body.ToString());

                myList.Add(nuevoItem);

                //myList.Add(new ItemForSend("jefe@test.com", "Tienes que hacer un test", "Esta es una evalucion que tenes que Revisar Capo"));

            }

            /*
             *  Aqui obtener los datos para el evaluador 
             *  usar dataaccess
             *
             */

            return myList;
        }
    }
}
