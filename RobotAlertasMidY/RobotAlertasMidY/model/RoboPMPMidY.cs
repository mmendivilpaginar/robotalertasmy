﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RobotAlertasMidY.model
{
    /// <summary>
    /// El Administrador/Coordinador de los pasos para enviar mails
    /// </summary>
   public class RoboPMPMidY
    {
       public interfaces.IProviderConnectionStaticMail ConnectionMail;
       private util.EnviarMail ManagerMail;

       public RoboPMPMidY(interfaces.IProviderConnectionStaticMail _connectionMail)
       {
           this.ConnectionMail = _connectionMail;
           this.ManagerMail = new util.EnviarMail(_connectionMail);
         

       }

       public void Run()
       {
          // this.EnviarReporteAEvaluadores();
          // this.EnviarReportesAEvaluados();

           EnviarReportesFYAdministrativos();
           EnviarReportesFYOperarios();
       }

       private void EnviarReportesAEvaluadosMY()
       {
           var rep = new ReportForEvaluado();
           var result = rep.GetBodyMail(9);
           foreach (var item in result)
           {
               this.ManagerMail.Enviar(item);
           }
       }

       private void EnviarReporteAEvaluadoresMY()
       {
           var rep = new ReportForEvaluador();
           var result=rep.GetBodyMail(9);
           foreach (var item in result )
           {
               this.ManagerMail.Enviar(item);               
           }
       }

       private void EnviarReportesFYAdministrativos()
       {
           var rep = new ReporteAdministrativoForEvaluadorFY();
           var result = rep.GetBodyMail(11);
           foreach (var item in result)
           {
               this.ManagerMail.Enviar(item);
           }

           var rep1 = new ReporteAdministrativoForEvaluadoFY();
           result = rep1.GetBodyMail(11);
           foreach (var item in result)
           {
               this.ManagerMail.Enviar(item);
           }
       }


       private void EnviarReportesFYOperarios()
       {
           var rep = new ReporteOperarioForEvaluado();
           var result = rep.GetBodyMail(10);
           foreach (var item in result)
           {
               this.ManagerMail.Enviar(item);
           }

           var rep1 = new ReporteOperarioForEvaluador();
            result = rep1.GetBodyMail(10);
           foreach (var item in result)
           {
               this.ManagerMail.Enviar(item);
           }

           var rep2 = new ReporteOperarioForAuditor();
           result = rep2.GetBodyMail(10);
           foreach (var item in result)
           {
               this.ManagerMail.Enviar(item);
           }
       }
    }
}
