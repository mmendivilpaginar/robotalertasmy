﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.Specialized;
using System.Configuration;

namespace RobotAlertasMidY.model
{
  public  class ValoresConfigMail:interfaces.IProviderConnectionStaticMail
    {

        private string GetValueConfig(string key)
        {
            NameValueCollection valores = ConfigurationManager.GetSection("Correo/Variables") as NameValueCollection;

            return valores[key];
        }

        #region IProviderConnectionStaticMail Members

        public string Port
        {
            get { 
                    return this.GetValueConfig("NT_Port");
                }
        }

        public bool EnableSsl
        {
            get { return Convert.ToBoolean(this.GetValueConfig("NT_EnableSsl")); }
        }

        public string User
        {
            get { return this.GetValueConfig("NT_User"); }
        }

        public string Password
        {
            get { return this.GetValueConfig("NT_Password"); }
        }

        public string SMTP_Address
        {
            get { return this.GetValueConfig("NT_Smtp"); }
        }

        public string From
        {
            get { return this.GetValueConfig("NT_From"); }
        }

        public string FromName
        {
            get { return this.GetValueConfig("NT_From_Name"); }
        }

        public string SiteUrl
        {
            get { return this.GetValueConfig("SiteUrl"); }
        }
        #endregion
    }
}
