﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RobotAlertasMidY.interfaces
{
    /// <summary>
    /// Contiene las propiedades estaticas de un correo.
    /// Propiedades que se configuran una unica vez y no durante el tiempo de ejecuion
    /// </summary>
    public interface IProviderConnectionStaticMail
    {
         string Port { get; }
         Boolean EnableSsl { get; }
         string User { get; }
         string Password { get; }
         string SMTP_Address { get; }
         string From { get; }
         string FromName { get; }
         string SiteUrl { get; }

    }
}
