﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RobotAlertasMidY.interfaces
{
    /// <summary>
    /// Contenido dinamico para un mail
    /// </summary>
    public interface IProviderContentDynamicMail
    {
         string Body { get;}
         string[] To { get; }
      
         string Asunto { get; }
         Boolean IsHtmlBody{ get;}



    }
}
